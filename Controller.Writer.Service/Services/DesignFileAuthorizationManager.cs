﻿using Controller.Writer.Tools.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using System.Web;

namespace Controller.Writer.Service.Services
{
    public class DesignFileAuthorizationManager : ServiceAuthorizationManager
    {
        private const string USER_PASSWORD = "Bonjour";

        private readonly SecureString CRYPTING_PASSWORD;

        public DesignFileAuthorizationManager()
        {
            string password = "12345abcde";
            CRYPTING_PASSWORD = password.Aggregate(new SecureString(), (ss, c) => { ss.AppendChar(c); return ss; });
        }

        protected override bool CheckAccessCore(OperationContext operationContext)
        {
            var authHeader = WebOperationContext.Current.IncomingRequest.Headers["Authorization"];

            if ((authHeader != null) && (authHeader != string.Empty))
            {
                var svcCredentials = System.Text.ASCIIEncoding.ASCII
                        .GetString(Convert.FromBase64String(authHeader.Substring(6))).Split(':');

                var user = new { Name = svcCredentials[0], Password = svcCredentials[1] };
                SecureString decipheredToken = CipherManager.Decrypt(user.Password, CRYPTING_PASSWORD);
                var pwd = Encoding.UTF8.GetString(CipherManager.ToByteArray(decipheredToken));
                var ticksPart = pwd.Substring(pwd.Length - 20);
                long ticks;
                if (long.TryParse(ticksPart, out ticks))
                {
                    return USER_PASSWORD.Equals(pwd.Substring(0, pwd.Length - 20))
                        //&& "Account007".Equals(user.Name)
                        && DateTime.UtcNow.Subtract(new TimeSpan(ticks)).Second < 5;
                }

            }
            //return base.CheckAccessCore(operationContext);
            return false;
        }
    }
}