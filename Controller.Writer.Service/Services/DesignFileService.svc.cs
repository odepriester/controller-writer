﻿using Controller.Writer.Service.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.ServiceModel.Web;

namespace Controller.Writer.Service.Services
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Service1" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select Service1.svc or Service1.svc.cs at the Solution Explorer and start debugging.
    public class DesignFileService : IDesignFileService
    {
        List<DesignFile> DesignFiles = null;
        public DesignFileService()
        {
            DesignFiles = new List<DesignFile>(3);
            DesignFiles.Add(new DesignFile() { Id = 1, Name = "Program 1 - Src", FileName = @"D:\GXProjects\STA1.gxw" });
            DesignFiles.Add(new DesignFile() { Id = 2, Name = "Program 2 - Src", FileName = @"D:\GXProjects\Test.gxw" });
            DesignFiles.Add(new DesignFile() { Id = 3, Name = "Program 3 - Src", FileName = @"D:\GXProjects\Test.Protected.gxw" });
            DesignFiles.Add(new DesignFile() { Id = 4, Name = "Program 4 - Wrong file", FileName = @"FileXXXX.gw2" });
            DesignFiles.Add(new DesignFile() { Id = 5, Name = "Program 5 - Zipped file", FileName = @"D:\GxWorksPrograms\PRG1.zip" });
            DesignFiles.ForEach(df => df.Uri = new System.Uri(
                $"{System.ServiceModel.OperationContext.Current.EndpointDispatcher.EndpointAddress.Uri}/designfile/{df.Id}"));
        }

        public Stream GetIndex()
        {
            MemoryStream stream = new MemoryStream();

            StreamWriter writer = new StreamWriter(stream, System.Text.Encoding.UTF8);
            writer.Write(Properties.Resources.DesignFileServiceIndex);
            writer.Flush();

            stream.Position = 0;
            //on indique le type de contenu
            WebOperationContext.Current.OutgoingResponse.ContentType = "text/html";
            return stream;
        }
        
        public DesignFiles GetAll()
        {
            return new DesignFiles(this.DesignFiles);
        }

        public DesignFile GetOne(string fileId)
        {
            int numericFileId;
            if (!int.TryParse(fileId, out numericFileId))
            {
                //throw new WebFaultException<string>($"No id given", System.Net.HttpStatusCode.BadRequest);
                throw new WebFaultException<DesignFile>(null, System.Net.HttpStatusCode.BadRequest);
            }
            try
            {
                DesignFile designFile = DesignFiles.FirstOrDefault(d => d.Id == numericFileId);
                if (designFile == null)
                {
                    //throw new WebFaultException<string>($"No design file found for id {fileId}", System.Net.HttpStatusCode.NotFound);
                    throw new WebFaultException<DesignFile>(null, System.Net.HttpStatusCode.NotFound);
                }
                designFile.Content = File.ReadAllBytes(designFile.FileName);
                return designFile;
            }
            catch (IOException)
            {
                //throw new WebFaultException<string>($"No program file found for id {fileId}", System.Net.HttpStatusCode.InternalServerError);
                throw new WebFaultException<DesignFile>(null, System.Net.HttpStatusCode.InternalServerError);
            }
        }
    }
}
