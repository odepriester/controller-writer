﻿using Controller.Writer.Service.Models;
using System.IO;
using System.ServiceModel;
using System.ServiceModel.Web;

namespace Controller.Writer.Service.Services
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IService1" in both code and config file together.
    [ServiceContract(Namespace ="http://baosongrobot.com/controller/writer/designfileservice", Name = "DesignFileService")]
    public interface IDesignFileService
    {
        [OperationContract]
        [WebGet(UriTemplate = "aide")]
        Stream GetIndex();

        [WebGet(UriTemplate = "designfiles", ResponseFormat = WebMessageFormat.Json)]
        [OperationContract]
        DesignFiles GetAll();

        [WebGet(UriTemplate = "designfile/{fileId}", ResponseFormat = WebMessageFormat.Json)]
        [OperationContract]
        DesignFile GetOne(string fileId);
    }
}
