﻿using System;
using System.Runtime.Serialization;

namespace Controller.Writer.Service.Models
{
    [DataContract(Namespace = "http://baosongrobot.com/controller/writer/designfileservice", Name = "DesignFile")]
    public class DesignFile
    {
        public DesignFile()
        {

        }

        [DataMember(Order = 1, IsRequired = true)]
        public int Id { get; set; }

        [DataMember(Order = 2)]
        public string Name { get; set; }

        [IgnoreDataMember]
        public string FileName { get; set; }

        [DataMember(Order = 3)]
        public Uri Uri { get; set; }

        [DataMember(EmitDefaultValue = false, Order = 4)]
        public byte[] Content { get; set; }

        public override string ToString()
        {
            return $"{this.GetType().ToString()} - {Id} - {Name}";
        }
    }
}
