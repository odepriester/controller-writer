﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Controller.Writer.Service.Models
{
    [CollectionDataContract(Namespace = "http://baosongrobot.com/controller/writer/designfileservice", Name = "DesignFiles")]
    public class DesignFiles:List<DesignFile>
    {
        public DesignFiles() { }

        public DesignFiles(IList<DesignFile> designFiles): base(designFiles)
        {

        }
    }
}
