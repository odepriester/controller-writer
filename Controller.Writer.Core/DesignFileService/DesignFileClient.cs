﻿using Controller.Writer.Core.Exceptions;
using Controller.Writer.Tools.Security;
using RestSharp;
using RestSharp.Authenticators;
using System;
using System.Linq;
using System.Security;
using System.Text;

namespace Controller.Writer.Core.DesignFileService
{
    public class DesignFileClient : IDesignFileClient
    {
        public Uri BaseUrl { get; }

        private readonly SecureString passwordSecureString;
        private readonly string _accountSid;
        private readonly SecureString _secretKey;

        public DesignFileClient(string accountSid, string secretKey, string baseUrl, string passphrase)
        {
            // Decrypt pass phrase based on current machine name
            passwordSecureString = CipherManager.Decrypt(passphrase, CipherManager.StringToSecureString(System.Environment.MachineName));
            _accountSid = accountSid;
            // Decrypt web service password
            _secretKey = CipherManager.Decrypt(secretKey, passwordSecureString);
            BaseUrl = new Uri(baseUrl);
        }

        /// <summary>
        /// Gets the token to authenticate web service calls
        /// </summary>
        /// <returns></returns>
        private string GetToken()
        {
            // Based on WS password + current time in number of ticks
            string token = $"{Encoding.UTF8.GetString(CipherManager.ToByteArray(_secretKey))}{DateTime.UtcNow.Ticks.ToString().PadLeft(20, '0')}";
            // Encrypt token
            return CipherManager.Encrypt(token, passwordSecureString);
        }

        /// <summary>
        /// Execute a WS call to retrieve a deserialized JSON Object
        /// </summary>
        /// <typeparam name="T">Local expected type</typeparam>
        /// <param name="request">Request to execute</param>
        /// <returns></returns>
        private T Execute<T>(RestRequest request) where T : new()
        {
            var client = new RestClient();
            client.BaseUrl = BaseUrl;
            client.Authenticator = new HttpBasicAuthenticator(_accountSid, GetToken());
            request.AddParameter("AccountSid", _accountSid, ParameterType.UrlSegment); // used on every request
            // Send request
            var response = client.Execute<T>(request);

            // Check response content
            if (response.ErrorException != null)
            {
                string message = $"Error retrieving response on call {request.Resource} with {string.Join(",", request.Parameters.Select(p => $"[{p.Name}:{p.Value}]"))}. StatusCode={response.StatusCode}.";
                var twilioException = new ControllerWriterException(message, response.ErrorException);
                throw twilioException;
            }
            else if (response.StatusCode != System.Net.HttpStatusCode.OK)
            {
                string message = $"Error retrieving response on call {request.Resource} with {string.Join(",", request.Parameters.Select(p => $"[{p.Name}:{p.Value}]"))}. StatusCode={response.StatusCode}.";
                var twilioException = new WebServiceCallException(message);
                throw twilioException;
            }
            return response.Data;
        }

        /// <summary>
        ///  Get all design files
        /// </summary>
        /// <returns></returns>
        public DesignFiles GetAll()
        {
            var request = new RestRequest();
            request.Resource = "designfiles";
            return this.Execute<DesignFiles>(request);
        }

        /// <summary>
        /// Get a design file with its file content
        /// </summary>
        /// <param name="fileId">Design file identifier</param>
        /// <returns></returns>
        public DesignFile GetDesignFile(int fileId)
        {
            var request = new RestRequest();
            request.Resource = "designfile/{fileId}";
            request.AddParameter("fileId", fileId, ParameterType.UrlSegment);
            return this.Execute<DesignFile>(request);
        }
    }
}
