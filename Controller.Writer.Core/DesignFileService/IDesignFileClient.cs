﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Controller.Writer.Core.DesignFileService
{
    public interface IDesignFileClient
    {
        DesignFiles GetAll();

        DesignFile GetDesignFile(int fileId);
    }
}
