﻿using System;
using System.Collections.Generic;

namespace Controller.Writer.Core.DesignFileService
{
    public class DesignFile : ICloneable, IEquatable<DesignFile>
    {
        public DesignFile() { }

        public int Id { get; set; }

        public string Name { get; set; }

        public Uri Uri { get; set; }

        public List<byte> Content { get; set; }

        public override string ToString()
        {
            return $"{this.GetType().ToString()} - {Id} - {Name}";
        }

        public object Clone()
        {
            return this.MemberwiseClone();
        }

        public bool Equals(DesignFile other)
        {
            return other != null
                && other.GetType().Equals(this.GetType())
                && other.Id.Equals(this.Id);
        }
    }
}
