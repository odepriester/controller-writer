﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Controller.Writer.Core.DesignFileService
{
    public class DesignFiles:List<DesignFile>
    {
        public DesignFiles() { }

        public DesignFiles(IList<DesignFile> designFiles): base(designFiles)
        {

        }
    }
}
