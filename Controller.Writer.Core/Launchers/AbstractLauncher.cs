﻿using log4net;
using System;

namespace Controller.Writer.Core.Launchers
{
    public abstract class AbstractLauncher<T> where T : AbstractLauncherParameter
    {
        protected static readonly ILog LOGGER = LogManager.GetLogger($"{typeof(AbstractLauncher<T>).Namespace}.AbstractLauncher");

        public event StepIncreaseEventHandler StepIncrease;

        public delegate void StepIncreaseEventHandler(object sender, StepIncreaseEventArgs e);

        public int Steps { get; protected set; }

        public abstract int MaximumSteps { get; }

        protected virtual void OnStepIncrease()
        {
            OnStepIncrease(1);
        }

        protected T LauncherParameter { get; }



        protected virtual void OnStepIncrease(int steps)
        {
            StepIncreaseEventArgs e = new StepIncreaseEventArgs() { Steps = steps };
            Steps += steps;
            StepIncrease?.Invoke(this, e);
        }
        public AbstractLauncher(T launcherParameter)
        {
            if (launcherParameter == null)
            {
                throw new Exceptions.ControllerWriterException("The parameter bean cannot be null");
            }
            LauncherParameter = launcherParameter;
            Steps = 0;
        }

        public void Run()
        {
            this.LauncherParameter.StartingTime = DateTime.Now;
            try
            {
                OnPreRun();
                OnStepIncrease();
                OnRun();
                OnStepIncrease();
                OnPostRun();
                OnStepIncrease();
            }
            finally
            {
                this.LauncherParameter.EndingTime = DateTime.Now;
            }

        }

        protected abstract void OnPostRun();

        protected abstract void OnRun();
        protected abstract void OnPreRun();
    }

    public class StepIncreaseEventArgs : EventArgs
    {
        public int Steps { get; set; }
    }

}
