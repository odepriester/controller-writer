﻿using Controller.Writer.Core.DesignFileService;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Controller.Writer.Core.Launchers
{
    public class GxWorksLauncherParameter : ProcessLauncherParameter
    {
        public DesignFile DesignFile { get; set; }

        public string GxWorksFileName { get; set; }
    }
}
