﻿using Controller.Writer.Core.DesignFileService;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security;
using System.Text;
using System.Threading.Tasks;

namespace Controller.Writer.Core.Launchers
{
    public class FTPLauncherParameter : AbstractLauncherParameter
    {
        /// <summary>
        /// Program to transfer
        /// </summary>
        public DesignFile DesignFile { get; set; }

        /// <summary>
        /// PLC IP Address
        /// </summary>
        public string PLCIPAddress { get; set; }

        /// <summary>
        /// PLC FTP User name
        /// </summary>
        public string PLCFTPUserName { get; set; }

        /// <summary>
        /// PLC FTP Password
        /// </summary>
        public SecureString PLCFTPPassword { get; set; }

        public FTPLauncherParameter()
        {
        }

        public override string ToString()
        {
            if (DesignFile != null)
            {
                return $"[{DesignFile.Id}-{DesignFile.Name}] to {PLCIPAddress}";
            }
            else
            {
                return $"[No program] to {PLCIPAddress}";
            }

        }
    }
}
