﻿using Controller.Writer.Core.DesignFileService;
using Controller.Writer.Core.Exceptions;
using log4net;
using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace Controller.Writer.Core.Launchers
{
    public class FTPLauncher : AbstractLauncher<FTPLauncherParameter>
    {
        protected new static readonly ILog LOGGER = LogManager.GetLogger(typeof(FTPLauncher));

        public FTPLauncher(FTPLauncherParameter launcherParameter) : base(launcherParameter)
        {
        }

        public override int MaximumSteps
        {
            get
            {
                return 3 + 2;
            }
        }

        protected override void OnPostRun()
        {
            LOGGER.Debug("OnPostRun");
            LOGGER.Info($"End transfer ${this.LauncherParameter.ToString()}");
        }

        /// <summary>
        /// Checks parameters
        /// </summary>
        protected override void OnPreRun()
        {
            LOGGER.Debug("OnPreRun");
            DesignFile programFile = this.LauncherParameter.DesignFile;
            if (programFile == null)
            {
                throw new ControllerWriterException("No program file to transfer");
            }
            if (programFile.Content == null || programFile.Content.Count == 0)
            {
                throw new ControllerWriterProgramException("No content", this.LauncherParameter.DesignFile);
            }

            if (string.IsNullOrWhiteSpace(this.LauncherParameter.PLCIPAddress))
            {
                throw new ControllerWriterException("No FTP IP address defined");
            }
            if (string.IsNullOrWhiteSpace(this.LauncherParameter.PLCFTPUserName))
            {
                throw new ControllerWriterException("No FTP user defined");
            }
            if (this.LauncherParameter.PLCFTPPassword == null || this.LauncherParameter.PLCFTPPassword.Length == 0)
            {
                LOGGER.Warn("No FTP password defined (may not be required)");
            }

            try
            {
                using (var response = GetRequestForRelativePath(string.Empty, WebRequestMethods.Ftp.ListDirectory).GetResponse())
                {
                }
            }
            catch (Exception ex) when (ex is WebException || ex is InvalidOperationException)
            {
                throw new ControllerWriterException($"Error while connecting PLC FTP ftp://{this.LauncherParameter.PLCFTPUserName}@{this.LauncherParameter.PLCIPAddress} : {ex.Message}");
            }

        }

        /// <summary>
        /// Gets the intialized FTP request to reach the PLC
        /// </summary>
        /// <param name="path">A relative path to the PLC FTP</param>
        /// <param name="action">A <see cref="WebRequestMethods.Ftp"/> value related to the action to do on the ftp</param>
        /// <returns>Initialized request</returns>
        private FtpWebRequest GetRequestForRelativePath(string path, string action)
        {
            if (path == "/")
            {
                path = string.Empty;
            }
            FtpWebRequest request = (FtpWebRequest)WebRequest.Create(new Uri($"ftp://{this.LauncherParameter.PLCIPAddress}/{path}"));
            request.Credentials = new NetworkCredential(this.LauncherParameter.PLCFTPUserName, this.LauncherParameter.PLCFTPPassword);
            request.Method = action;
            request.UseBinary = true;
            request.KeepAlive = false;
            request.UsePassive = false;
            request.Timeout = 5000;
            return request;
        }

#if FTP_LOCALHOST
        /// <summary>
        /// List the format patterns a file name will have depending on its extension
        /// </summary>
        private static readonly IDictionary<String, String> EXTENSION_PATTERNS = new Dictionary<String, String>() {
            { ".qpg", "{0}" },
            { ".qpa", "{0}" },
            { ".dat", "{0}" }
        };
#else
        /// <summary>
        /// List the format patterns a file name will have depending on its extension
        /// </summary>
        private static readonly IDictionary<String, String> EXTENSION_PATTERNS = new Dictionary<String, String>() {
            { ".qpg", "0:{0}" },
            { ".qpa", "0:{0}" },
            { ".dat", "4:{0}" }
        };
#endif
        /// <summary>
        /// Gets the name the file will have on the PLC
        /// Warning : file name defines storage location
        /// 0:xx stands for device memory
        /// 2:xx stands for SD Memory Card
        /// 3:xx stands for standard RAM
        /// 4:xx stands for standard ROM
        /// The pattern applied is listed in <see cref="EXTENSION_PATTERNS"/>. If the original file name extension is not known, exception occurs
        /// </summary>
        /// <param name="fileName">Original file name</param>
        /// <returns>File name after transfer</returns>
        /// <exception cref="ControllerWriterException">Occurs when the file extension has no known pattern</exception>
        private static string GetTargetFileName(string fileName)
        {
            string extension = Path.GetExtension(fileName).ToLowerInvariant();
            try
            {
                return string.Format(EXTENSION_PATTERNS[extension], fileName.ToUpperInvariant());
            }
            catch (KeyNotFoundException)
            {
                throw new ControllerWriterException($"No pattern found for {extension} from {fileName}");
            }

        }

        private bool TransferFileToPLC(ZipArchiveEntry localArchiveEntry, string targetFileName)
        {
            LOGGER.Debug($"OnRun - Transfer {localArchiveEntry.Name} [{localArchiveEntry.Length}b] to {targetFileName}");
            FtpWebRequest request = GetRequestForRelativePath(targetFileName, WebRequestMethods.Ftp.UploadFile);
            int intBufferLength = 16 * 1024;
            byte[] objBuffer = new byte[intBufferLength];
            // Read the file
            using (Stream localFileStream = localArchiveEntry.Open())
            {
                if (localFileStream is DeflateStream)
                {
                    // When a zip is used, the main stream has no length.
                    // The deflated related stream must be used to retrieve this information
                    request.ContentLength = ((DeflateStream)localFileStream).BaseStream.Length;
                }
                else
                {
                    request.ContentLength = localFileStream.Length;
                }
                try
                {
                    using (Stream objStream = request.GetRequestStream())
                    {
                        int len = 0;
                        // Transfert data
                        while ((len = localFileStream.Read(objBuffer, 0, intBufferLength)) != 0)
                        {
                            // Write file Content 
                            objStream.Write(objBuffer, 0, len);
                        }
                    }
                }
                catch (WebException ex)
                {
                    throw new ControllerWriterProgramException($"{localArchiveEntry.Name}/{targetFileName} : {ex.Message}", this.LauncherParameter.DesignFile);
                }
            }
            return true;
        }



        protected override void OnRun()
        {
            LOGGER.Debug("OnRun");
            using (MemoryStream zipStream = new MemoryStream(this.LauncherParameter.DesignFile.Content.ToArray(), false))
            {
                try
                {
                    ZipArchive zipArchive = new ZipArchive(zipStream, ZipArchiveMode.Read, false);

                    LOGGER.Debug("OnRun - Check archive content");
                    OnStepIncrease();
                    #region Check if all files within the zip are known
                    List<string> errorMessages = new List<string>();
                    List<KeyValuePair<ZipArchiveEntry, string>> dataToTransfer = new List<KeyValuePair<ZipArchiveEntry, string>>(zipArchive.Entries.Count);
                    foreach (ZipArchiveEntry entry in zipArchive.Entries)
                    {
                        // Check if all the files can be uploaded and retrieve their file name
                        try
                        {
                            LOGGER.Debug($"OnRun - Check {entry.Name}");
                            dataToTransfer.Add(new KeyValuePair<ZipArchiveEntry, string>(entry, GetTargetFileName(entry.Name)));
                        }
                        catch (ControllerWriterException ex)
                        {
                            // Unknown file extension --> Error
                            errorMessages.Add(ex.Message);
                        }
                    }
                    if (errorMessages.Count > 0)
                    {
                        // If any error, build one global message and throw a global exception
                        StringBuilder sbError = new StringBuilder((errorMessages.Count + 1) * 50);
                        sbError.AppendLine("The extensions below are unknown:");
                        errorMessages.ForEach(message => sbError.AppendLine(message));
                        throw new ControllerWriterProgramException(sbError.ToString(), this.LauncherParameter.DesignFile);
                    }
                    #endregion
                    LOGGER.Debug("OnRun - Transfer starting");
                    OnStepIncrease();
                    foreach (var transferItem in dataToTransfer)
                    {
                        // Transfer files on PLC
                        TransferFileToPLC(transferItem.Key, transferItem.Value);
                    }
                }
                catch (InvalidDataException)
                {
                    // Stream content is not a valid zip file
                    throw new ControllerWriterProgramException("Content is not a valid zip file", this.LauncherParameter.DesignFile);
                }
            }
        }
    }
}
