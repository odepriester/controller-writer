﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Controller.Writer.Core.Launchers
{
    public abstract class AbstractLauncherParameter
    {
        public DateTime StartingTime { get; internal set; }
        public DateTime EndingTime { get; internal set; }
    }
}
