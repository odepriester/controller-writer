﻿using System;
using System.Diagnostics;
using System.IO;
using Controller.Writer.Core.Exceptions;

namespace Controller.Writer.Core.Launchers
{
    /// <summary>
    /// Area to set application location
    /// </summary>
    public class Rectangle
    {
        public int X { get; set; }
        public int Y { get; set; }
        public int Width { get; set; }
        public int Height { get; set; }

        public static Rectangle Of(int x, int y, int width, int height)
        {
            return new Rectangle { X = x, Y = y, Width = width, Height = height };
        }
    }

    public abstract class ProcessLauncher<T> : AbstractLauncher<T> where T : ProcessLauncherParameter
    {
        public ProcessLauncher(T launcherParameter) : base(launcherParameter)
        {

            if (string.IsNullOrWhiteSpace(launcherParameter.ExecutableFileName))
            {
                throw new GxWorksException("No application filename.");
            }
            else if (!File.Exists(launcherParameter.ExecutableFileName))
            {
                throw new GxWorksException($"{launcherParameter.ExecutableFileName} does not exist.");
            }
        }

        /// <summary>
        /// Launch an application et returns the process created
        /// </summary>
        /// <param name="fileName">Executable file name to run</param>
        /// <param name="arguments">Args of the application</param>
        /// <param name="applicationArea">Application location and size</param>
        /// <returns>Process created</returns>
        protected Process Launch(String fileName, String arguments, Rectangle applicationArea)
        {

            const uint NORMAL_PRIORITY_CLASS = 0x0020;
            const uint CREATE_UNICODE_ENVIRONMENT = 0x0400;
            var pInfo = new Kernel32.PROCESS_INFORMATION();
            var sInfo = new Kernel32.STARTUPINFO();
            if (applicationArea != null)
            {
                sInfo.dwX = (uint)applicationArea.X; // X Position 
                sInfo.dwY = (uint)applicationArea.Y; // Y Position 
                sInfo.dwXSize = (uint)applicationArea.Width; // Width
                sInfo.dwYSize = (uint)applicationArea.Height; // Height
            }
            sInfo.dwFlags = (uint)Kernel32.STARTF.STARTF_USEPOSITION | (uint)Kernel32.STARTF.STARTF_USESIZE;
            var pSec = new Kernel32.SECURITY_ATTRIBUTES();
            var tSec = new Kernel32.SECURITY_ATTRIBUTES();
            pSec.nLength = System.Runtime.InteropServices.Marshal.SizeOf(pSec);
            tSec.nLength = System.Runtime.InteropServices.Marshal.SizeOf(tSec);

            var create_result = Kernel32.CreateProcess(
                fileName,                   // lpApplicationName
                " " + arguments,            // lpCommandLine
                ref pSec,                   // lpProcessAttributes
                ref tSec,                   // lpThreadAttributes
                false,                      // bInheritHandles
                NORMAL_PRIORITY_CLASS | CREATE_UNICODE_ENVIRONMENT,      // dwCreationFlags
                IntPtr.Zero,                // lpEnvironment
                null,                       // lpCurrentDirectory
                ref sInfo,                  // lpStartupInfo
                out pInfo);                 // lpProcessInformation


            return Process.GetProcessById(pInfo.dwProcessId);
        }

        protected Process Launch()
        {
            return this.Launch(this.LauncherParameter.ExecutableFileName, this.LauncherParameter.ProcessArguments, this.GetApplicationLocation());
        }

        /// <summary>
        /// Get the application location and size
        /// </summary>
        /// <returns></returns>
        protected abstract Rectangle GetApplicationLocation();
    }
}
