﻿using Controller.Writer.Core.DesignFileService;
using Controller.Writer.Core.Exceptions;
using log4net;
using System;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using System.Windows.Automation;
using TestStack.White;
using TestStack.White.Configuration;
using TestStack.White.UIItems;
using TestStack.White.UIItems.Finders;
using TestStack.White.UIItems.WindowItems;

namespace Controller.Writer.Core.Launchers
{
    public class GxWorksLauncher : ProcessLauncher<GxWorksLauncherParameter>
    {
        #region Window Titles
        /// <summary>
        /// Authentication popup title
        /// </summary>
        private const string TITLE_AUTHENTICATION_POPUP = "User Authentication";

        /// <summary>
        /// Write to PLC popup title
        /// </summary>
        private const string TITLE_WRITE_TO_PLC_POPUP = "Online Data Operation";

        #endregion

        private static Rectangle APPLICATION_AREA = Rectangle.Of(0, 0, 100, 50);

        public override int MaximumSteps
        {
            get
            {
                return 5;
            }
        }


        public GxWorksLauncher(GxWorksLauncherParameter launcherParameter):base(launcherParameter)
        {
            launcherParameter.ProcessArguments = launcherParameter.GxWorksFileName;
        }

        protected override Rectangle GetApplicationLocation()
        {
            return APPLICATION_AREA;
        }


        /// <summary>
        /// Run the scenario for given file
        /// </summary>
        /// <param name="gxWorksPlanFileName">GXWorks Plan full path</param>
        private void RunScenario(string gxWorksPlanFileName, DesignFile designFile)
        {
            LOGGER.Info($"Write {designFile.Id}-{designFile.Name} - Start");
            Window mainWindow = null;
            OnStepIncrease();
            // Launch the application and gets the process
            Process ps = Launch();
            OnStepIncrease();
            // Attach the process to a TestStack application in order to be manipulated easily
            CoreAppXmlConfiguration.Instance.FindWindowTimeout = 5000;
            using (Application application = Application.Attach(ps))
            {
                try
                {
                    mainWindow = application.GetWindows().First();
                    if (mainWindow == null)
                    {
                        // No window found : should not happen
                        throw new GxWorksWindowNotFoundException("Main windows of the application not found");
                    }
                    CheckAuthentication(mainWindow);
                    Thread.Sleep(200);
                    OnStepIncrease();
                    ValidateWarningPopup(mainWindow);
                    Thread.Sleep(200);
                    OnStepIncrease();
                    WriteToPLC(mainWindow);
                    OnStepIncrease();
                }
                catch (GxWorksException e)
                {
                    LOGGER.Error(e.Message);
                    throw;
                }
                catch (Exception e) when (e is AutomationException || e is WhiteException)
                {
                    LOGGER.Error(e.Message, e.InnerException);
                    throw new GxWorksException(e.Message, e.InnerException);
                }
                finally
                {
                    LOGGER.Info("Run scenario - Close application - Start");
                    CloseProject(mainWindow);
                    application.Close(); // close the window and the application
                    LOGGER.Info("Run scenario - Close application - End");
                    OnStepIncrease(MaximumSteps - Steps);
                    LOGGER.Info($"Write {designFile.Id}-{designFile.Name} - End");
                }
            }
        }

        private static void ValidateWarningPopup(Window mainWindow)
        {
            Window popupWarning = mainWindow.ModalWindows().FirstOrDefault(w => w.Name == "MELSOFT Series GX Works2");
            if (popupWarning != null)
            {
                var text = popupWarning.Get<Label>(SearchCriteria.ByFramework(WindowsFramework.Win32.ToString()));
                LOGGER.Warn($"Warning Popup Found : [{text.Name}]");
                Button okButton = popupWarning.Get<Button>(SearchCriteria.ByFramework(WindowsFramework.Win32.ToString()));
                okButton.Click();
                Thread.Sleep(200);
                ValidateWarningPopup(mainWindow);
            }
        }


        /// <summary>
        /// Check the authentication
        /// </summary>
        /// <param name="mainWindow"></param>
        private static void CheckAuthentication(Window mainWindow)
        {
            LOGGER.Info("Run scenario - Check authentication - Start");
            var popup = mainWindow.ModalWindows().FirstOrDefault(w => w.Title == TITLE_AUTHENTICATION_POPUP);
            if (popup == null)
            {
                throw new GxWorksException("Run scenario - Check authentication - Not a protected file");
            }
            // var listTextBox = popup.GetMultiple(TestStack.White.UIItems.Finders.SearchCriteria.ByControlType(System.Windows.Automation.ControlType.Text));
            var listTextBox = popup.GetMultiple(SearchCriteria.ByFramework(WindowsFramework.Win32.ToString()));
            // User name
            listTextBox.First(t => t.AutomationElement.Current.AutomationId == "1000" && t is TextBox).SetValue("Operator");
            // Password
            listTextBox.First(t => t.AutomationElement.Current.AutomationId == "1001" && t is TextBox).SetValue("songbao");
            // OK button click
            ((Button)listTextBox.First(t => t.AutomationElement.Current.AutomationId == "1" && t is Button)).Click();
            LOGGER.Info("Run scenario - Check authentication - End");
        }

        private static void OpenWaitForPopupToClose(Window mainWindow, string title)
        {
            var popup = mainWindow.ModalWindows().FirstOrDefault(w => w.Title == title);
            bool exist = popup != null;
            if (exist)
            {
                // Gets the dialog box window handle to check if it is alive
                var windowHandle = popup.AutomationElement.Current.NativeWindowHandle;
                while (exist)
                {
                    // Check every 1s if the window is still open
                    Thread.Sleep(1000);
                    exist = Kernel32.IsWindowVisible(new IntPtr(windowHandle));
                }
            }
        }

        /// <summary>
        /// Write the project on the PLC
        /// </summary>
        /// <param name="mainWindow"></param>
        private static void WriteToPLC(Window mainWindow)
        {
            LOGGER.Info("Run scenario - Write to PLC - Start");
            if (mainWindow != null)
            {
                #region Send Ctrl + Shift + P as a shortcut to write into the PLC
                // We use this method because it is faster than looking for the PLC button
                mainWindow.Keyboard.HoldKey(TestStack.White.WindowsAPI.KeyboardInput.SpecialKeys.CONTROL);
                mainWindow.Keyboard.HoldKey(TestStack.White.WindowsAPI.KeyboardInput.SpecialKeys.SHIFT);
                mainWindow.Keyboard.Enter("p");
                mainWindow.Keyboard.LeaveKey(TestStack.White.WindowsAPI.KeyboardInput.SpecialKeys.CONTROL);
                mainWindow.Keyboard.LeaveKey(TestStack.White.WindowsAPI.KeyboardInput.SpecialKeys.SHIFT);
                #endregion

                #region Check if PLC dialog box has been open and wait for the user to close it
                try
                {
                    var popup = mainWindow.ModalWindows().FirstOrDefault(w => w.Title == TITLE_WRITE_TO_PLC_POPUP);
                    bool exist = popup != null;
                    if (exist)
                    {
                        // Gets the dialog box window handle to check if it is alive
                        var windowHandle = popup.AutomationElement.Current.NativeWindowHandle;
                        while (exist)
                        {
                            // Check every 1s if the window is still open
                            Thread.Sleep(1000);
                            exist = Kernel32.IsWindowVisible(new IntPtr(windowHandle));
                        }
                        LOGGER.Info("Run scenario - Write to PLC - End");
                    }
                    else
                    {
                        throw new GxWorksException("Error while writing : the \"Write to PLC\" dialog box could not be opened");
                    }
                }
                catch (ElementNotAvailableException)
                {
                    LOGGER.Warn("User has closed the \"Write To PLC\" dialog box to quickly");
                }
                #endregion
            }
        }

        /// <summary>
        /// Close a project
        /// </summary>
        /// <param name="mainWindow"></param>
        private static void CloseProject(Window mainWindow)
        {
            if (mainWindow != null)
            {
                #region Send Ctrl + Shift + W as a shortcut to close the project
                mainWindow.Keyboard.HoldKey(TestStack.White.WindowsAPI.KeyboardInput.SpecialKeys.CONTROL);
                mainWindow.Keyboard.HoldKey(TestStack.White.WindowsAPI.KeyboardInput.SpecialKeys.SHIFT);
                mainWindow.Keyboard.Enter("w");
                mainWindow.Keyboard.LeaveKey(TestStack.White.WindowsAPI.KeyboardInput.SpecialKeys.CONTROL);
                mainWindow.Keyboard.LeaveKey(TestStack.White.WindowsAPI.KeyboardInput.SpecialKeys.SHIFT);
                #endregion

                #region Closing confirmation popup : TAB then RETURN to close
                mainWindow.KeyIn(TestStack.White.WindowsAPI.KeyboardInput.SpecialKeys.TAB);
                mainWindow.KeyIn(TestStack.White.WindowsAPI.KeyboardInput.SpecialKeys.RETURN);
                #endregion
            }
        }

        protected override void OnPostRun()
        {
        }

        protected override void OnRun()
        {
            this.RunScenario(LauncherParameter.GxWorksFileName, LauncherParameter.DesignFile);
        }

        protected override void OnPreRun()
        {
        }
    }
}