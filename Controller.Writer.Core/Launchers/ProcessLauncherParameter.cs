﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Controller.Writer.Core.Launchers
{
    public abstract class ProcessLauncherParameter:AbstractLauncherParameter
    {
        public string ExecutableFileName { get; set; }

        public string ProcessArguments { get; set; }

    }
}
