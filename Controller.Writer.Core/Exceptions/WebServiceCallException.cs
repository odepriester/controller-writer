﻿using System;

namespace Controller.Writer.Core.Exceptions
{
    public class WebServiceCallException:GxWorksException
    {
        public WebServiceCallException(string message, Exception innerException) : base(message, innerException) { }

        public WebServiceCallException(string message) : base(message) { }
    }
}
