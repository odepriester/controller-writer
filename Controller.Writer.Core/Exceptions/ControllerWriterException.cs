﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Controller.Writer.Core.Exceptions
{
    [Serializable]
    public class ControllerWriterException : ApplicationException
    {
        public ControllerWriterException(string message, Exception innerException) : base(message, innerException) { }

        public ControllerWriterException(string message) : base(message) { }
    }
}
