﻿using System;

namespace Controller.Writer.Core.Exceptions
{
    public class GxWorksWindowNotFoundException:GxWorksException
    {
        public GxWorksWindowNotFoundException(string message, Exception innerException) : base(message, innerException) { }

        public GxWorksWindowNotFoundException(string message) : base(message) { }
    }
}
