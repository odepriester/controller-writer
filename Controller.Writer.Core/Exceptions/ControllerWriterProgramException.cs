﻿using Controller.Writer.Core.DesignFileService;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Controller.Writer.Core.Exceptions
{
    [Serializable]
    public class ControllerWriterProgramException : ControllerWriterException
    {
        private readonly DesignFile Program;

        public override string Message
        {
            get
            {
                if (Program != null)
                {
                    return $"{Program.Id} - {Program.Name} : {base.Message}";
                }
                else
                {
                    return base.Message;
                }

            }
        }

        public ControllerWriterProgramException(string message, Exception innerException, DesignFile program) : base(message, innerException)
        {
            Program = program.Clone() as DesignFile;
        }

        public ControllerWriterProgramException(string message, DesignFile program) : base(message)
        {
            Program = program.Clone() as DesignFile;
        }
    }
}
