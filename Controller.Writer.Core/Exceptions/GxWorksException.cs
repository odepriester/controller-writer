﻿using System;

namespace Controller.Writer.Core.Exceptions
{
    [Serializable]
    public class GxWorksException:ControllerWriterException
    {
        public GxWorksException(string message, Exception innerException) : base(message, innerException) { }

        public GxWorksException(string message) : base(message) { }
    }
}
