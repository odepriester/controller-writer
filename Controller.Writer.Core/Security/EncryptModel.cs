﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security;
using System.Text;
using System.Threading.Tasks;

namespace Controller.Writer.Core.Security
{
    public class EncryptModel
    {
        public SecureString value { get; set; }

        public String encryptedValue { get; internal set; }
    }
}
