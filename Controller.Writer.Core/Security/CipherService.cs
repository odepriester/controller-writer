﻿using Controller.Writer.Tools.Security;
using System.Security;

namespace Controller.Writer.Core.Security
{
    /// <summary>
    /// Ciphering service
    /// </summary>
    /// <remarks>Made only to avoid exposing the Controller.Writer.Tools assembly to project referencing the Core</remarks>
    public static class CipherService
    {
        /// <summary>
        /// Sets the encrypted value of model from its unencrypted value
        /// </summary>
        /// <param name="model">Model to encrypt</param>
        /// <param name="password">Password to use</param>
        public static void EncryptModel(EncryptModel model, SecureString password)
        {
            model.encryptedValue = CipherManager.Encrypt(model.value, password);
        }

        /// <summary>
        /// Sets the encrypted value of model from its unencrypted value
        /// Password used : the current machine name
        /// </summary>
        /// <param name="model">Model to encrypt</param>
        public static void EncryptModel(EncryptModel model)
        {
            EncryptModel(model, CipherManager.StringToSecureString(System.Environment.MachineName));
        }
    }
}
