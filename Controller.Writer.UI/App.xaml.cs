﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;

namespace Controller.Writer.UI
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        private void Application_Startup(object sender, StartupEventArgs e)
        {
            Window openWindow;
            if (e.Args.Length > 0 && e.Args[0] == "/setup")
            {
                openWindow = new SetupEncrypter();
            }
            else

            {
                openWindow = new MainWindow();
            }
            openWindow.Show();
        }
    }
}
