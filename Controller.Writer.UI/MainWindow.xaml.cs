﻿using Controller.Writer.Core.DesignFileService;
using Controller.Writer.Core.Exceptions;
using Controller.Writer.Core.Launchers;
using Controller.Writer.UI.Log4Net;
using log4net;
using log4net.Config;
using log4net.Core;
using System;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Input;

namespace Controller.Writer.UI
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window, IAsyncAppenderAware
    {
        #region Fields
        /// <summary>
        /// Logger
        /// </summary>
        private static readonly ILog log = LogManager.GetLogger(typeof(MainWindow));

        /// <summary>
        /// Callback component used to refresh the progress bar
        /// </summary>
        private Progress<int> progress;

        /// <summary>
        /// Web service client to get the design files
        /// </summary>
        private IDesignFileClient designFileClient;

#if GXWORKS
        /// <summary>
        /// Gx Works Exe full path
        /// </summary>
        private string gxWorksFileName = string.Empty;

        /// <summary>
        /// Location of the downloaded design file
        /// </summary>
        private string temporaryFilesPath = string.Empty;
#endif
        private bool AutoScroll = true;

        #endregion

        /// <summary>
        /// Logging method called by the async appender
        /// </summary>
        /// <param name="loggingEvent"></param>
        public void Log(LoggingEvent loggingEvent)
        {
            log.Logger.Log(loggingEvent);
        }

        /// <summary>
        /// Initialize the window
        /// </summary>
        public MainWindow()
        {
            // Log4Net configuration
            XmlConfigurator.Configure();
            InitializeComponent();
            log.Info("Application start");
            InitializeWindowData();
        }

        /// <summary>
        /// Initialize the current window parameters
        /// </summary>
        private void InitializeWindowData()
        {
#if GXWORKS
            ButtonSendToPLC.Click += ButtonSendToPLC_Click;
            ButtonSendToPLC.Visibility = Visibility.Visible;

            #region GX Works configuration
            // Gets the GX Works exe full file name 
            gxWorksFileName = Properties.Settings.Default.GxWorksFullPathName;
            if (string.IsNullOrWhiteSpace(gxWorksFileName))
            {
                log.Error($"Property GxWorksFullPathName has not been set");
            }
            if (!File.Exists(gxWorksFileName))
            {
                log.Error($"Property GxWorksFullPathName value refers to a non existing path");
            }
            Process.GetProcessesByName("GD2").ToList().ForEach(p => p.Kill());
            #endregion

            #region Temporary files location
            string subDirectoryPath = Properties.Settings.Default.TemporaryDesignFilePath;
            if (string.IsNullOrWhiteSpace(subDirectoryPath))
            {
                log.Warn($"Property TemporaryDesignFilePath has not been set");
            }
            temporaryFilesPath = $"{Path.GetTempPath()}\\{subDirectoryPath}";
            if (!Directory.Exists(temporaryFilesPath))
            {
                Directory.CreateDirectory(temporaryFilesPath);
            }

            //accessControl = directory.GetAccessControl();
            //var rules = accessControl.GetAccessRules(true, true, typeof(System.Security.Principal.SecurityIdentifier));
            //var owner = accessControl.GetOwner(typeof(System.Security.Principal.SecurityIdentifier));
            //var currentUser = WindowsIdentity.GetCurrent().User;
            //List<FileSystemAccessRule> rulesToRemove = new List<FileSystemAccessRule>();
            //List<FileSystemAccessRule> rulesToKeep = new List<FileSystemAccessRule>();
            //foreach (FileSystemAccessRule rule in rules)
            //{
            //    if (rule.IdentityReference.Equals(currentUser))
            //    {
            //        rulesToRemove.Add(new FileSystemAccessRule(
            //            rule.IdentityReference,
            //            FileSystemRights.FullControl,
            //            AccessControlType.Allow));
            //        rulesToKeep.Add(new FileSystemAccessRule(
            //            rule.IdentityReference,
            //            FileSystemRights.FullControl,
            //            AccessControlType.Deny));

            //    }
            //    else if (rule.IdentityReference.Equals(owner))
            //    {
            //        rulesToKeep.Add(new FileSystemAccessRule(
            //            rule.IdentityReference,
            //            FileSystemRights.FullControl,
            //            AccessControlType.Allow));
            //    }
            //}
            //accessControl.SetAccessRuleProtection(true, false);
            //rulesToRemove.ForEach(rule => accessControl.RemoveAccessRule(rule));
            //rulesToKeep.ForEach(rule => accessControl.AddAccessRule(rule));
            //directory.SetAccessControl(accessControl);

            #endregion
#endif
            #region Design file service configuration

#if STUB
            designFileClient = new StubDesignFileClient();
            this.Title += "[STUB VERSION]";
#else
            string baseUrl = Properties.Settings.Default.DesignFileServiceBaseUrl;
            if (string.IsNullOrWhiteSpace(baseUrl))
            {
                log.Error($"Property DesignFileServiceBaseUrl has not been set");
            }
            else
            {
                log.Debug($"Design file client BaseUrl={baseUrl}");
                string accountSid = Properties.Settings.Default.DesignFileServiceAccountSId;
                string password = Properties.Settings.Default.DesignFileServiceSecret;
                designFileClient = new DesignFileClient(accountSid, password, baseUrl, Properties.Settings.Default.Passphrase);
            }
#endif
            if (designFileClient != null)
            {
                // Gets the design file list
                InitializeDesignList();
            }
            #endregion

            #region Other minor initializations
            ProgressBarLoading.Value = 0;
            // Initializes the async process that will refresh the progress bar
            progress = new Progress<int>(value => ProgressBarLoading.Value = (value == 0 ? 0 : ProgressBarLoading.Value + value));
            
            this.WebBrowserHelp.Navigate(new Uri(String.Format("file:///{0}/Help/Controller.Writer.Real.html", Directory.GetCurrentDirectory())));
            #endregion

#if FTP_LOCALHOST
            TextBoxIPAddress.Text = "127.0.0.1";
            TextBoxUserName.Text = "MELSEC";
            PasswordBoxPassword.Password = "MELSEC";
#else
            TextBoxIPAddress.Text = Properties.Settings.Default.FTPPLCDefaultAddress;
            TextBoxUserName.Text = Properties.Settings.Default.FTPPLCDefaultUserName;
            PasswordBoxPassword.Password = Properties.Settings.Default.FTPPLCDefaultPassword;
#endif
        }

        /// <summary>
        /// Initialize the available PLC List
        /// </summary>
        private void InitializeDesignList()
        {
            log.Info("Get program files list - Start");
            try
            {
                DesignFiles designFiles = designFileClient.GetAll();
                ListViewDesignFiles.ItemsSource = designFiles.OrderBy(d => d.Name);
                CollectionView view = (CollectionView)CollectionViewSource.GetDefaultView(ListViewDesignFiles.ItemsSource);
                if (view != null)
                {
                    view.Filter = ProgramFilter;
                }
            }
            catch (ControllerWriterException ex)
            {
                log.Error("Program files list could not be loaded.", ex);
            }
            log.Info("Get program files list - End");
        }

        /// <summary>
        /// Send the selected program to the PLC via FTP
        /// </summary>
        private async void SendToPLCwithFTP()
        {
            this.Cursor = Cursors.Wait;
            ListViewDesignFiles.IsEnabled = false;
            ButtonSendToPLC.IsEnabled = false;

            ((IProgress<int>)progress).Report(0);
            // If no item selected --> Go out without error
            if (ListViewDesignFiles.SelectedItem != null)
            {

                try
                {
                    int fileId = (ListViewDesignFiles.SelectedItem as DesignFile).Id;

                    log.Info($"Get design file Id:{fileId} - Start");
                    // Gets the file from server
                    DesignFile file = designFileClient.GetDesignFile(fileId);
                    log.Info($"Get design file Id:{fileId} - End");
                    if (file == null)
                    {
                        throw new ControllerWriterException($"DesignFile {fileId} not found");
                    }
                    log.Info($"FTP Launcher Id:{fileId} - start");
                    FTPLauncherParameter param = new FTPLauncherParameter()
                    {
                        PLCIPAddress = TextBoxIPAddress.Text,
                        PLCFTPUserName = TextBoxUserName.Text,
                        PLCFTPPassword = PasswordBoxPassword.SecurePassword,
                        DesignFile = file
                    };
                    FTPLauncher launcher = new FTPLauncher(param);
                    launcher.StepIncrease += new FTPLauncher.StepIncreaseEventHandler(Launcher_StepIncrease);
                    ProgressBarLoading.Maximum = launcher.MaximumSteps;
                    // Run the GX Works scenario
                    await Task.Run(() =>
                    {
                        launcher.Run();
                    });
                    log.Info($"FTP Launcher Id:{fileId} - End");

                }

                catch (ControllerWriterException ex)
                {
                    log.Error(ex.Message);
                }
            }
            this.Cursor = Cursors.Arrow;
            ListViewDesignFiles.IsEnabled = true;
            ButtonSendToPLC.IsEnabled = true;
        }

#if GXWORKS
        /// <summary>
        /// Retrieve a design file content then store it as file and give its full path
        /// </summary>
        /// <param name="designFile">Design file from the web service</param>
        /// <returns>Path to the file storing the <paramref name="designFile "/>content</returns>
        /// <exception cref="GxWorksException">File content is empty. Means an error occured on server side</exception>
        private string StoreDesignFileContent(DesignFile designFile)
        {
            string filePath = string.Empty;
            if (designFile.Content != null && designFile.Content.Count > 0)
            {
                // Defines file full path
                filePath = $"{temporaryFilesPath}\\{Guid.NewGuid().ToString().Substring(0, 25)}.gxw";
                // Store the data into this file
                File.WriteAllBytes(filePath, designFile.Content.ToArray());
            }
            else
            {
                throw new GxWorksException($"The design file for {designFile.Id} - {designFile.Name} has no content");
            }
            return filePath;
        }


        /// <summary>
        /// Send the selected program to the PLC
        /// </summary>
        [Obsolete("Replaced by the FTP transfer laucher")]
        private async void SendToPlc()
        {
            this.Cursor = Cursors.Wait;
            ListViewDesignFiles.IsEnabled = false;
            ButtonSendToPLC.IsEnabled = false;

            string temporaryDesignFileName = string.Empty;
            ((IProgress<int>)progress).Report(0);
            // If no item selected --> Go out without error
            if (ListViewDesignFiles.SelectedItem == null)
            {
                return;
            }

            // Gets the selected item file Id
            int fileId = (ListViewDesignFiles.SelectedItem as DesignFile).Id;
            try
            {
                log.Info($"Get design file Id:{fileId} - Start");
                // Gets the file from server
                DesignFile file = designFileClient.GetDesignFile(fileId);
                log.Info($"Get design file Id:{fileId} - End");
                if (file == null)
                {
                    throw new GxWorksException($"DesignFile {fileId} not found");
                }
                temporaryDesignFileName = StoreDesignFileContent(file);

        #region Launch Gx Works and run the scenario
                log.Info($"Gx Works launcher Id:{fileId} - Start");
                // Initialization
                GxWorksLauncherParameter launcherParameter = new GxWorksLauncherParameter()
                {
                    DesignFile = file,
                    GxWorksFileName = temporaryDesignFileName,
                    ExecutableFileName = gxWorksFileName
                };
                GxWorksLauncher launcher = new GxWorksLauncher(launcherParameter);
                // Bind the progress bar event
                launcher.StepIncrease += new GxWorksLauncher.StepIncreaseEventHandler(Launcher_StepIncrease);
                ProgressBarLoading.Maximum = launcher.MaximumSteps;
                // Run the GX Works scenario
                await Task.Run(() =>
                {
                    launcher.Run();
                });
                log.Info($"Gx Works launcher Id:{fileId} - End");
        #endregion
            }
            catch (WebServiceCallException ex)
            {
                log.Error(ex.Message);
            }
            catch (GxWorksException ex)
            {
                log.Error(ex);
            }
            catch (AggregateException ex)
            {
                log.Error(ex.InnerException);
            }
            catch (Exception ex)
            {
                log.Error(ex);
            }
            finally
            {
                // Remove the temporary file
                if (!string.IsNullOrEmpty(temporaryDesignFileName))
                {
                    File.Delete(temporaryDesignFileName);
                }
            }
            this.Cursor = Cursors.Arrow;
            ListViewDesignFiles.IsEnabled = true;
            ButtonSendToPLC.IsEnabled = true;
        }
        /// <summary>
        /// Event : Send the selected file to the PLC
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        [Obsolete("Replaced by the FTP transfer laucher")]
        private void ButtonSendToPLC_Click(object sender, RoutedEventArgs e)
        {

            SendToPlc();
        }
#endif

        /// <summary>
        /// Event : A new step has been reached in the scenario
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Launcher_StepIncrease(object sender, StepIncreaseEventArgs e)
        {
            // Call the progress bar refresh
            ((IProgress<int>)progress).Report(e.Steps);
        }

        /// <summary>
        /// Predicate to determinate wether an item has to be displayed in the list or not
        /// </summary>
        /// <param name="item">List item</param>
        /// <returns>true if item has to be dislayed</returns>
        private bool ProgramFilter(object item)
        {
            bool result = true;
            // No filter value
            if (String.IsNullOrEmpty(TextBoxFilterDesignFiles.Text))
                return result;
            else
            {
                string sourceText = TextBoxFilterDesignFiles.Text;
                // Split the terms
                var sourceTerms = sourceText.Split(' ');
                foreach (var sourceTerm in sourceTerms)
                {
                    // If filter contains un "id:" term, this term will apply directly to the id and not on the name
                    if (sourceTerm.StartsWith("id:", StringComparison.OrdinalIgnoreCase) && sourceTerm.Length > 3)
                    {
                        int id;
                        if (int.TryParse(sourceTerm.Split(':')[1], out id))
                        {
                            result &= (item as DesignFile).Id.Equals(id);
                        }
                    }
                    else
                    {
                        // Filter on DesignFile.Name
                        result &= ((item as DesignFile).Name.IndexOf(sourceTerm, StringComparison.OrdinalIgnoreCase) >= 0);
                    }
                }
            }
            return result;
        }

        /// <summary>
        /// Refresh the listview when the 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void TextBoxFilterDesignFiles_TextChanged(object sender, System.Windows.Controls.TextChangedEventArgs e)
        {
            CollectionViewSource.GetDefaultView(ListViewDesignFiles.ItemsSource).Refresh();
        }

        private void ListViewDesignFiles_MouseDoubleClick(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            SendToPLCwithFTP();
        }

        /// <summary>
        /// Make the output scroll down while filling
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ScrollViewer_ScrollChanged(object sender, System.Windows.Controls.ScrollChangedEventArgs e)
        {
            ScrollViewer scrollViewer = sender as ScrollViewer;
            // User scroll event : set or unset auto-scroll mode
            if (e.ExtentHeightChange == 0)
            {   // Content unchanged : user scroll event
                if (scrollViewer.VerticalOffset == scrollViewer.ScrollableHeight)
                {   // Scroll bar is in bottom
                    // Set auto-scroll mode
                    AutoScroll = true;
                }
                else
                {   // Scroll bar isn't in bottom
                    // Unset auto-scroll mode
                    AutoScroll = false;
                }
            }

            // Content scroll event : auto-scroll eventually
            if (AutoScroll && e.ExtentHeightChange != 0)
            {   // Content changed and auto-scroll mode set
                // Autoscroll
                scrollViewer.ScrollToVerticalOffset(scrollViewer.ExtentHeight);
            }
        }

        /// <summary>
        /// Empty the output
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ButtonClearConsole_Click(object sender, RoutedEventArgs e)
        {
            this.TextBlockOutput.Inlines.Clear();
        }

        /// <summary>
        /// Manage the listview "Program Name" column list resize
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ListViewDesignFiles_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            double listViewActualWidth = (sender as ListView).ActualWidth;
            if (listViewActualWidth > ColumnDesignFileId.Width + 10)
            {
                ColumnDesignFileName.Width = listViewActualWidth - ColumnDesignFileId.Width - 10;
            }
        }

        /// <summary>
        /// Manage the left panel autosize
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void GridSplitter_MouseDoubleClick(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            GridSplitter splitter = sender as GridSplitter;
            if (splitter.Tag == null)
            {
                splitter.Tag = GridColumnLeft.Width;
                GridColumnLeft.Width = new GridLength(0, GridUnitType.Pixel);
            }
            else if (splitter.Tag is GridLength)
            {
                GridColumnLeft.Width = (GridLength)splitter.Tag;
                splitter.Tag = null;
            }
        }

        private void ButtonSendToPLCFTP_Click(object sender, RoutedEventArgs e)
        {
            SendToPLCwithFTP();
        }

        private void Expander_Expanded(object sender, RoutedEventArgs e)
        {
            WebBrowserHelp.Width = PanelRight.ActualWidth;
        }

        private void ButtonRefreshProgramList_Click(object sender, RoutedEventArgs e)
        {
            InitializeDesignList();
        }
    }
}
