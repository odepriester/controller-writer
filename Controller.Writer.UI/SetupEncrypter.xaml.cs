﻿using Controller.Writer.Core.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Controller.Writer.UI
{
    /// <summary>
    /// Interaction logic for SetupEncrypter.xaml
    /// </summary>
    public partial class SetupEncrypter : Window
    {
        internal SetupEncrypterViewModel CurrentModel;

        public SetupEncrypter()
        {
            InitializeComponent();
            CurrentModel = new SetupEncrypterViewModel()
            {
                PassPhraseModel = new EncryptModel(),
                WSPasswordModel = new EncryptModel()
            };
        }

        private void ButtonEncrypt_Click(object sender, RoutedEventArgs e)
        {
            CurrentModel.PassPhraseModel.value = PasswordBoxPassPhrase.SecurePassword;
            CurrentModel.WSPasswordModel.value = PasswordBoxWSPassword.SecurePassword;
            CipherService.EncryptModel(CurrentModel.PassPhraseModel);
            CipherService.EncryptModel(CurrentModel.WSPasswordModel, CurrentModel.PassPhraseModel.value);
            TextBoxEncryptedPassPhrase.Text = CurrentModel.PassPhraseModel.encryptedValue;
            TextBoxEncryptedWSPassowrd.Text = CurrentModel.WSPasswordModel.encryptedValue;
        }

        private void ButtonSave_Click(object sender, RoutedEventArgs e)
        {
            
        }
    }

    internal class SetupEncrypterViewModel
    {
        internal EncryptModel PassPhraseModel { get; set; }
        internal EncryptModel WSPasswordModel { get; set; }
    }
}
