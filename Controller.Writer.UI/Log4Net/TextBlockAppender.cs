﻿using log4net.Appender;
using log4net.Core;
using log4net.Util;
using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Media;
using static log4net.Appender.ColoredConsoleAppender;

namespace Controller.Writer.UI.Log4Net
{
    /// <summary>
    /// Log4Net appender to log into a given TextBlock
    /// Color codes are managed as <see cref="ColoredConsoleAppender"/> except <see cref="ColoredConsoleAppender.Colors.HighIntensity"/>
    /// Colors cannot be combined
    /// </summary>
    public class TextBlockAppender : AppenderSkeleton
    {
        /// <summary>
        /// Target TextBlock
        /// </summary>
        private TextBlock _textBlock;

        /// <summary>
        /// TextBlock name.
        /// <para>Defined into the log4net configuration. If none, the appender will not be used</para>
        /// </summary>
        public string TextBlockName { get; set; }
        /// <summary>
        /// TextBlock parent window name.
        /// <para>Defined into the log4net configuration. If none, the main window is used</para>
        /// </summary>
        public string FormName { get; set; }

        /// <summary>
        /// Level mapping - Defines the colors for logging levels
        /// <para>Defined into the log4net configuration. If none, Foreground is Black and Background is White</para>
        /// </summary>
        protected LevelMapping levelMapping = new LevelMapping();

        /// <summary>
        /// Mapping methode to fill the levelMapping with the configuration file content
        /// </summary>
        /// <param name="mapping">1 mapping from the configuration file</param>
        public void AddMapping(LevelColors mapping)
        {
            levelMapping.Add(mapping);
        }

        public override void ActivateOptions()
        {
            base.ActivateOptions();
            levelMapping.ActivateOptions();
        }

        protected override void Append(LoggingEvent loggingEvent)
        {
            if (_textBlock == null)
            {
                // No textBlock configured : logger cannot be used
                if (String.IsNullOrEmpty(TextBlockName))
                    return;
                // Find the form of the targetted textblock
                Window form = null;
                if (!String.IsNullOrEmpty(FormName))
                {
                    form = Application.Current.MainWindow.FindName(FormName) as Window;
                } else
                {
                    // Not configured = main window
                    form = Application.Current.MainWindow;
                }
                // No form found : logger cannot be used
                if (form == null)
                    return;

                // Find the textBlock
                _textBlock = Application.Current.MainWindow.FindName(TextBlockName) as TextBlock;
                if (_textBlock == null)
                    return;
                
                // Free the reference on window closing
                form.Closing += (s, e) => _textBlock = null;
            }
            // Write the log
            Run line = new Run(RenderLoggingEvent(loggingEvent));
            // Get the configured colors
            LevelColors levelColors = levelMapping.Lookup(loggingEvent.Level) as LevelColors;
            if (levelColors != null)
            {
                // Transform the colors to Brush
                line.Foreground = TransformColorToBrush(levelColors.ForeColor);
                line.Background = TransformColorToBrush(levelColors.BackColor);
            }
            _textBlock.Inlines.Add(line);
        }

        /// <summary>
        /// Convert a ColoredConsoleAppender.Colors into a Brush
        /// </summary>
        /// <param name="color"></param>
        /// <returns>SolidColorBrush</returns>
        private static Brush TransformColorToBrush(ColoredConsoleAppender.Colors color)
        {
            Brush result = Brushes.Black;
            switch (color)
            {
                case ColoredConsoleAppender.Colors.Blue:
                    result = Brushes.Blue;
                    break;
                case ColoredConsoleAppender.Colors.Green:
                    result = Brushes.Green;
                    break;
                case ColoredConsoleAppender.Colors.Red:
                    result = Brushes.DarkRed;
                    break;
                case ColoredConsoleAppender.Colors.White:
                    result = Brushes.White;
                    break;
                case ColoredConsoleAppender.Colors.Yellow:
                    result = Brushes.Orange;
                    break;
                case ColoredConsoleAppender.Colors.Purple:
                    result = Brushes.Purple;
                    break;
                case ColoredConsoleAppender.Colors.Cyan:
                    result = Brushes.Cyan;
                    break;
                case ColoredConsoleAppender.Colors.HighIntensity:
                    result = Brushes.Red;
                    break;
                default:
                    result = Brushes.Black;
                    break;
            }
            return result;
        }
    }
}
