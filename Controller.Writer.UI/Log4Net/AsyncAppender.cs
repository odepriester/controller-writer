﻿using log4net.Appender;
using log4net.Core;
using log4net.Util;
using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Media;
using static log4net.Appender.ColoredConsoleAppender;

namespace Controller.Writer.UI.Log4Net
{
    /// <summary>
    /// Log4Net appender to log into a given TextBlock
    /// Color codes are managed as <see cref="ColoredConsoleAppender"/> except <see cref="ColoredConsoleAppender.Colors.HighIntensity"/>
    /// Colors cannot be combined
    /// </summary>
    public class AsyncAppender : AppenderSkeleton
    {
        /// <summary>
        /// TextBlock parent window name.
        /// <para>Defined into the log4net configuration. If none, the main window is used</para>
        /// </summary>
        public string FormName { get; set; }


        protected override void Append(LoggingEvent loggingEvent)
        {

            Window form = null;
            if (!String.IsNullOrEmpty(FormName))
            {
                form = Application.Current.MainWindow.FindName(FormName) as Window;
            }
            else
            {
                Application.Current.Dispatcher.Invoke(() =>
                {
                    form = Application.Current.MainWindow;
                    if (form == null)
                        return;
                    if (form is IAsyncAppenderAware)
                    {
                        (form as IAsyncAppenderAware).Log(loggingEvent);
                    }
                });
                // Not configured = main window
                
            }
        }
    }
}
