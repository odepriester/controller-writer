﻿using log4net.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Controller.Writer.UI.Log4Net
{
    public interface IAsyncAppenderAware
    {
        void Log(LoggingEvent loggingEvent);
    }
}
