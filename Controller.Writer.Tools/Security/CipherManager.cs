﻿using System;
using System.IO;
using System.Linq;
using System.Net;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Security;
using System.Security.Cryptography;
using System.Text;

namespace Controller.Writer.Tools.Security
{
    public class CipherManager
    {
        // This constant is used to determine the keysize of the encryption algorithm in bits.
        // We divide this by 8 within the code below to get the equivalent number of bytes.
        private const int Keysize = 256;

        // This constant determines the number of iterations for the password bytes generation function.
        private const int DerivationIterations = 4096;

        private static byte[] Generate256BitsOfRandomEntropy()
        {
            var randomBytes = new byte[32]; // 32 Bytes will give us 256 bits.
            using (var rngCsp = new RNGCryptoServiceProvider())
            {
                // Fill the array with cryptographically secure random bytes.
                rngCsp.GetBytes(randomBytes);
            }
            return randomBytes;
        }

        /// <summary>
        /// Extract the byte arrary from a SecureString
        /// Warning : This method result MUST be used into a local context so prevent from security leaks
        /// </summary>
        /// <param name="ss"></param>
        /// <returns></returns>
        public static byte[] ToByteArray(SecureString ss)
        {
            IntPtr bstr1 = IntPtr.Zero;
            RuntimeHelpers.PrepareConstrainedRegions();

            try
            {
                bstr1 = Marshal.SecureStringToBSTR(ss);
                byte[] result = new byte[ss.Length];
                unsafe
                {
                    int i = 0;
                    for (Char* ptr1 = (Char*)bstr1.ToPointer();
                        *ptr1 != 0;
                         ++ptr1)
                    {
                        result[i++] = Convert.ToByte(*ptr1);
                    }
                }
                return result;
            }
            finally
            {
                if (bstr1 != IntPtr.Zero)
                {
                    Marshal.ZeroFreeBSTR(bstr1);
                }
            }
        }

        public static string Encrypt(SecureString plainText, SecureString passPhrase)
        {
            return Encrypt(ToByteArray(plainText), passPhrase);
        }

        public static string Encrypt(string plainText, SecureString passPhrase)
        {
            return Encrypt(Encoding.UTF8.GetBytes(plainText), passPhrase);
        }

        /// <summary>
        /// Encrypt data with a given pass phrase
        /// </summary>
        /// <param name="plainTextBytes">Data as bytes array</param>
        /// <param name="passPhrase">Passphrase to encrypt</param>
        /// <returns>An encrypted and Base64 encoded string</returns>
        public static string Encrypt(byte[] plainTextBytes, SecureString passPhrase)
        {
            // Salt and IV is randomly generated each time, but is preprended to encrypted cipher text
            // so that the same Salt and IV values can be used when decrypting.  
            var saltStringBytes = Generate256BitsOfRandomEntropy();
            var ivStringBytes = Generate256BitsOfRandomEntropy();

            using (var password = new Rfc2898DeriveBytes(new NetworkCredential("", passPhrase).Password, saltStringBytes, DerivationIterations))
            {
                var keyBytes = password.GetBytes(Keysize / 8);
                using (var symmetricKey = new RijndaelManaged())
                {
                    symmetricKey.BlockSize = 256;
                    symmetricKey.Mode = CipherMode.CBC;
                    symmetricKey.Padding = PaddingMode.PKCS7;
                    using (var encryptor = symmetricKey.CreateEncryptor(keyBytes, ivStringBytes))
                    {
                        using (var memoryStream = new MemoryStream())
                        {
                            using (var cryptoStream = new CryptoStream(memoryStream, encryptor, CryptoStreamMode.Write))
                            {
                                cryptoStream.Write(plainTextBytes, 0, plainTextBytes.Length);
                                cryptoStream.FlushFinalBlock();
                                // Create the final bytes as a concatenation of the random salt bytes, the random iv bytes and the cipher bytes.
                                var cipherTextBytes = saltStringBytes;
                                cipherTextBytes = cipherTextBytes.Concat(ivStringBytes).ToArray();
                                cipherTextBytes = cipherTextBytes.Concat(memoryStream.ToArray()).ToArray();
                                return Convert.ToBase64String(cipherTextBytes);
                            }
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Builds a <see cref="SecureString"/> from a string
        /// </summary>
        /// <param name="value">String to securise</param>
        /// <returns></returns>
        public static SecureString StringToSecureString(string value)
        {
            if (!string.IsNullOrEmpty(value))
            {
                return value.Aggregate(new SecureString(), (ss, c) => { ss.AppendChar(c); return ss; });
            }
            else
            {
                return null;
            }
        }

        public static SecureString Decrypt(string cipherText, SecureString passPhrase)
        {
            // Get the complete stream of bytes that represent:
            // [32 bytes of Salt] + [32 bytes of IV] + [n bytes of CipherText]
            var cipherTextBytesWithSaltAndIv = Convert.FromBase64String(cipherText);
            // Get the saltbytes by extracting the first 32 bytes from the supplied cipherText bytes.
            var saltStringBytes = cipherTextBytesWithSaltAndIv.Take(Keysize / 8).ToArray();
            // Get the IV bytes by extracting the next 32 bytes from the supplied cipherText bytes.
            var ivStringBytes = cipherTextBytesWithSaltAndIv.Skip(Keysize / 8).Take(Keysize / 8).ToArray();
            // Get the actual cipher text bytes by removing the first 64 bytes from the cipherText string.
            var cipherTextBytes = cipherTextBytesWithSaltAndIv.Skip((Keysize / 8) * 2).Take(cipherTextBytesWithSaltAndIv.Length - ((Keysize / 8) * 2)).ToArray();

            using (var password = new Rfc2898DeriveBytes(new NetworkCredential("", passPhrase).Password, saltStringBytes, DerivationIterations))
            {
                var keyBytes = password.GetBytes(Keysize / 8);
                using (var symmetricKey = new RijndaelManaged())
                {
                    symmetricKey.BlockSize = 256;
                    symmetricKey.Mode = CipherMode.CBC;
                    symmetricKey.Padding = PaddingMode.PKCS7;
                    using (var decryptor = symmetricKey.CreateDecryptor(keyBytes, ivStringBytes))
                    {
                        using (var memoryStream = new MemoryStream(cipherTextBytes))
                        {
                            using (var cryptoStream = new CryptoStream(memoryStream, decryptor, CryptoStreamMode.Read))
                            {
                                var plainTextBytes = new byte[cipherTextBytes.Length];
                                var decryptedByteCount = cryptoStream.Read(plainTextBytes, 0, plainTextBytes.Length);
                                unsafe
                                {
                                    fixed (char* p = Encoding.UTF8.GetChars(plainTextBytes, 0, decryptedByteCount))
                                    {
                                        return new SecureString(p, decryptedByteCount);
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}
