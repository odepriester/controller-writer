﻿namespace TestControlApplication
{
    partial class TestStackForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.GroupBoxDesignSelection = new System.Windows.Forms.GroupBox();
            this.ComboBoxDesignSelection = new System.Windows.Forms.ComboBox();
            this.DesignBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.ButtonSendToPLC = new System.Windows.Forms.Button();
            this.PanelMessages = new System.Windows.Forms.Panel();
            this.StatusStripMain = new System.Windows.Forms.StatusStrip();
            this.ToolStripStatusLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this.ToolStripProgressBarPLC = new System.Windows.Forms.ToolStripProgressBar();
            this.GroupBoxOutput = new System.Windows.Forms.GroupBox();
            this.webBrowser1 = new System.Windows.Forms.WebBrowser();
            this.GroupBoxDesignSelection.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DesignBindingSource)).BeginInit();
            this.StatusStripMain.SuspendLayout();
            this.GroupBoxOutput.SuspendLayout();
            this.SuspendLayout();
            // 
            // GroupBoxDesignSelection
            // 
            this.GroupBoxDesignSelection.Controls.Add(this.ComboBoxDesignSelection);
            this.GroupBoxDesignSelection.Controls.Add(this.ButtonSendToPLC);
            this.GroupBoxDesignSelection.Location = new System.Drawing.Point(0, 134);
            this.GroupBoxDesignSelection.Name = "GroupBoxDesignSelection";
            this.GroupBoxDesignSelection.Size = new System.Drawing.Size(617, 54);
            this.GroupBoxDesignSelection.TabIndex = 3;
            this.GroupBoxDesignSelection.TabStop = false;
            this.GroupBoxDesignSelection.Text = "PLC Design selection";
            // 
            // ComboBoxDesignSelection
            // 
            this.ComboBoxDesignSelection.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ComboBoxDesignSelection.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.ComboBoxDesignSelection.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.ComboBoxDesignSelection.DataSource = this.DesignBindingSource;
            this.ComboBoxDesignSelection.DisplayMember = "Name";
            this.ComboBoxDesignSelection.FormattingEnabled = true;
            this.ComboBoxDesignSelection.Location = new System.Drawing.Point(12, 19);
            this.ComboBoxDesignSelection.Name = "ComboBoxDesignSelection";
            this.ComboBoxDesignSelection.Size = new System.Drawing.Size(497, 21);
            this.ComboBoxDesignSelection.TabIndex = 4;
            this.ComboBoxDesignSelection.ValueMember = "Id";
            // 
            // DesignBindingSource
            // 
            this.DesignBindingSource.AllowNew = false;
            this.DesignBindingSource.Sort = "Name";
            // 
            // ButtonSendToPLC
            // 
            this.ButtonSendToPLC.Location = new System.Drawing.Point(530, 19);
            this.ButtonSendToPLC.Name = "ButtonSendToPLC";
            this.ButtonSendToPLC.Size = new System.Drawing.Size(75, 23);
            this.ButtonSendToPLC.TabIndex = 3;
            this.ButtonSendToPLC.Text = "Send to PLC";
            this.ButtonSendToPLC.UseVisualStyleBackColor = true;
            this.ButtonSendToPLC.Click += new System.EventHandler(this.ButtonSendToPLC_Click);
            // 
            // PanelMessages
            // 
            this.PanelMessages.Dock = System.Windows.Forms.DockStyle.Top;
            this.PanelMessages.Location = new System.Drawing.Point(0, 0);
            this.PanelMessages.Name = "PanelMessages";
            this.PanelMessages.Size = new System.Drawing.Size(617, 128);
            this.PanelMessages.TabIndex = 5;
            // 
            // StatusStripMain
            // 
            this.StatusStripMain.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ToolStripStatusLabel,
            this.ToolStripProgressBarPLC});
            this.StatusStripMain.Location = new System.Drawing.Point(0, 571);
            this.StatusStripMain.Name = "StatusStripMain";
            this.StatusStripMain.Size = new System.Drawing.Size(617, 22);
            this.StatusStripMain.TabIndex = 6;
            this.StatusStripMain.Text = "statusStrip1";
            // 
            // ToolStripStatusLabel
            // 
            this.ToolStripStatusLabel.AutoSize = false;
            this.ToolStripStatusLabel.Name = "ToolStripStatusLabel";
            this.ToolStripStatusLabel.Size = new System.Drawing.Size(100, 17);
            this.ToolStripStatusLabel.Text = "Ready";
            // 
            // ToolStripProgressBarPLC
            // 
            this.ToolStripProgressBarPLC.Name = "ToolStripProgressBarPLC";
            this.ToolStripProgressBarPLC.Size = new System.Drawing.Size(100, 16);
            this.ToolStripProgressBarPLC.Style = System.Windows.Forms.ProgressBarStyle.Continuous;
            // 
            // GroupBoxOutput
            // 
            this.GroupBoxOutput.Controls.Add(this.webBrowser1);
            this.GroupBoxOutput.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.GroupBoxOutput.Location = new System.Drawing.Point(0, 194);
            this.GroupBoxOutput.Name = "GroupBoxOutput";
            this.GroupBoxOutput.Size = new System.Drawing.Size(617, 377);
            this.GroupBoxOutput.TabIndex = 7;
            this.GroupBoxOutput.TabStop = false;
            this.GroupBoxOutput.Text = "Output";
            // 
            // webBrowser1
            // 
            this.webBrowser1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.webBrowser1.Location = new System.Drawing.Point(3, 16);
            this.webBrowser1.MinimumSize = new System.Drawing.Size(20, 20);
            this.webBrowser1.Name = "webBrowser1";
            this.webBrowser1.Size = new System.Drawing.Size(611, 358);
            this.webBrowser1.TabIndex = 0;
            // 
            // TestStackForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(617, 593);
            this.Controls.Add(this.GroupBoxOutput);
            this.Controls.Add(this.StatusStripMain);
            this.Controls.Add(this.GroupBoxDesignSelection);
            this.Controls.Add(this.PanelMessages);
            this.Name = "TestStackForm";
            this.Text = "TestStackForm";
            this.Load += new System.EventHandler(this.TestStackForm_Load);
            this.GroupBoxDesignSelection.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.DesignBindingSource)).EndInit();
            this.StatusStripMain.ResumeLayout(false);
            this.StatusStripMain.PerformLayout();
            this.GroupBoxOutput.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.GroupBox GroupBoxDesignSelection;
        private System.Windows.Forms.ComboBox ComboBoxDesignSelection;
        private System.Windows.Forms.Button ButtonSendToPLC;
        private System.Windows.Forms.Panel PanelMessages;
        private System.Windows.Forms.BindingSource DesignBindingSource;
        private System.Windows.Forms.StatusStrip StatusStripMain;
        private System.Windows.Forms.ToolStripProgressBar ToolStripProgressBarPLC;
        private System.Windows.Forms.ToolStripStatusLabel ToolStripStatusLabel;
        private System.Windows.Forms.GroupBox GroupBoxOutput;
        private System.Windows.Forms.WebBrowser webBrowser1;
    }
}