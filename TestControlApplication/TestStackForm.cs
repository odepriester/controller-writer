﻿using Controller.Writer.Core.Exceptions;
using Controller.Writer.Core.Launchers;
using Controller.Writer.Core.PLCDesigns;
using System;
using System.IO;
using System.Linq;
using System.Windows.Forms;
using System.Xml.Serialization;

namespace TestControlApplication
{
    public partial class TestStackForm : Form
    {
        private const String GXWORKS_PATH = @"C:\Program Files (x86)\MELSOFT\GPPW2\GD2.exe";

        public TestStackForm()
        {
            InitializeComponent();
            InitializeDesignList();
        }

        private void InitializeDesignList()
        {
            using (FileStream fileStream = new FileStream(Properties.Settings.Default.PLCDesignListFile, FileMode.Open))
            {
                XmlSerializer serializer = new XmlSerializer(typeof(DesignList));
                DesignList designList = (DesignList)serializer.Deserialize(fileStream);
                DesignBindingSource.DataSource = designList.Designs.OrderBy(d => d.Name);
            }
        }

        //private void test()
        //{
        //    String fileName = @"""D:\GXProjects\MyWorkspace\Test1\Project.gd2""";
        //    String pathWithFileName = $"{GXWORKS_PATH} {fileName}";
        //    ProcessStartInfo ps = new ProcessStartInfo(GXWORKS_PATH, fileName);
        //    ps.WindowStyle = ProcessWindowStyle.Normal;
        //    using (TestStack.White.Application application = TestStack.White.Application.Launch(ps))
        //    {
        //        try
        //        {
        //            Window mainWindow = application.GetWindows().First();
        //            //TestStack.White.UIItems.MenuItems.Menu menuProject = mainWindow.MenuBar.MenuItem("Project", "Close");
        //            //menuProject.ChildMenus.ForEach(cm => treeViewWindows.Nodes.Add(cm.Name));

        //            //windows.ForEach(w => treeViewWindows.Nodes.Add(new TreeNode(w.Name, w.Items.ConvertAll(m => new TreeNode(m.Name)).ToArray())));
        //            TestStack.White.UIItems.Button buttonSendToPLC = mainWindow.Get<TestStack.White.UIItems.Button>("Write to PLC");
        //            ////System.Diagnostics.Debug.WriteLine(buttonSendToPLC.Id);
        //            for (int i = 0; i < 3; i++)
        //            {

        //                buttonSendToPLC.Click();
        //                Thread.Sleep(2000);
        //            }
        //            TestStack.White.UIItems.MenuItems.Menu menuClosePlan = mainWindow.MenuBar.MenuItem("Project", "Close");
        //            menuClosePlan.Click();
        //            Window popup = mainWindow.ModalWindows().First(mw => mw.Name.StartsWith("MELSOFT Series"));

        //            popup.Get<TestStack.White.UIItems.Button>("Yes").Click();
        //            //windows.ForEach(w => treeViewWindows.Nodes.Add(new TreeNode(w.Name, w.Items.ConvertAll(m => new TreeNode(m.Name)).ToArray())));
        //        }
        //        catch (Exception ex)
        //        {
        //            System.Diagnostics.Debug.WriteLine(ex.Message);
        //        }
        //        finally
        //        {
        //            //application.Close();
        //        }
        //    }
        //}

        private void TestStackForm_Load(object sender, EventArgs e)
        {

        }

        private void ButtonSendToPLC_Click(object sender, EventArgs e)
        {
            string gxWorksFileName = Properties.Settings.Default.GxWorksFullPathName;
            string fileName = (ComboBoxDesignSelection.SelectedItem as Design).FileName;
            webBrowser1.DocumentText = @"<html><body><strong>coucou</coucou></body></html>";
            try
            {
                GxWorksLauncher launcher = new GxWorksLauncher(gxWorksFileName);
                launcher.RunScenario(fileName);
            }
            catch (GxWorksException ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.RetryCancel, MessageBoxIcon.Error);
            }
        }

        private void statusStrip1_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {

        }
    }
}
