# controller.writer
Module to transfer Mitsubishi GxWorks programs to PLC

This project is made to do the following things
* Get the programs list from a REST web service
* After having selected a program, download the program ZIP archive from the same web service
* Uncompress the archive
* Copy the program files to the PLC via FTP

