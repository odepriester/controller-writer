﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Controller.Writer.Tools.Security;
using System.Security;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Linq;

namespace Controller.Writer.Tests
{
    [TestClass]
    public unsafe class CipherManagerTest
    {
        [TestMethod]
        public void AreEncryptDecryptSymmetric()
        {
            
            const string password = "abcdefghijklmnopqrstuvwxyz";
            const string originalWord = "Bonjour";
            SecureString passwordSecureString = CipherManager.StringToSecureString(password);
            SecureString originalSecureString = CipherManager.StringToSecureString(originalWord);
            passwordSecureString.MakeReadOnly();
            originalSecureString.MakeReadOnly();
            // Encrypt
            string cipheredWord = CipherManager.Encrypt(originalWord, passwordSecureString);
            System.Diagnostics.Debug.WriteLine($"Ciphered word : {cipheredWord}");
            // Decrypt
            SecureString uncipheredWord = CipherManager.Decrypt(cipheredWord, passwordSecureString);
            // Eval if equal
            Assert.IsTrue(SecureStringEqual(originalSecureString, uncipheredWord));
        }

        /// <summary>
        /// Checks that 2 SecureString are equal
        /// </summary>
        /// <param name="s1"></param>
        /// <param name="s2"></param>
        /// <returns></returns>
        private bool SecureStringEqual(SecureString s1, SecureString s2)
        {
            if (s1 == null)
            {
                throw new ArgumentNullException("s1");
            }
            if (s2 == null)
            {
                throw new ArgumentNullException("s2");
            }

            if (s1.Length != s2.Length)
            {
                return false;
            }
            IntPtr bstr1 = IntPtr.Zero;
            IntPtr bstr2 = IntPtr.Zero;

            RuntimeHelpers.PrepareConstrainedRegions();

            try
            {
                bstr1 = Marshal.SecureStringToBSTR(s1);
                bstr2 = Marshal.SecureStringToBSTR(s2);

                unsafe
                {
                    for (Char* ptr1 = (Char*)bstr1.ToPointer(), ptr2 = (Char*)bstr2.ToPointer();
                        *ptr1 != 0 && *ptr2 != 0;
                         ++ptr1, ++ptr2)
                    {
                        if (*ptr1 != *ptr2)
                        {
                            return false;
                        }
                    }
                }

                return true;
            }
            finally
            {
                if (bstr1 != IntPtr.Zero)
                {
                    Marshal.ZeroFreeBSTR(bstr1);
                }

                if (bstr2 != IntPtr.Zero)
                {
                    Marshal.ZeroFreeBSTR(bstr2);
                }
            }
        }
    }
}
